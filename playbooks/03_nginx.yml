#!/usr/bin/env ansible-playbook
#
# @description Install Nginx
#
# Run with:
# ./playbooks/03_nginx.yml
#

- hosts: www
  remote_user: root
  gather_facts: no
  handlers:
    - import_tasks: ../handlers/nginx.yml
  tasks:
    - name: Adds nginx ppa
      apt_repository:
        repo: "ppa:nginx/stable"
    - apt_key:
        url: http://keyserver.ubuntu.com:11371/pks/lookup?op=get&search=0x00A6F0A3C300EE8C
        state: present
    - name: Adds certbot ppa
      apt_repository:
        repo: "ppa:certbot/certbot"
    - name: Installs nginx+certbot
      apt:
        name:
          - nginx-full
          - certbot
        state: latest
        update_cache: yes
      register: __nginx_just_installed
    - name: Removes nginx default server link
      file:
        path: /etc/nginx/sites-enabled/default
        state: absent
    - name: Removes nginx default server
      file:
        path: /etc/nginx/sites-available/default
        state: absent
      notify: Restart nginx

    - name: Add certificate renewal cronjob
      when: cert_domain is defined
      cron:
        name: "Renew letsencrypt certificates"
        special_time: daily
        job: "certbot renew --pre-hook 'service nginx stop' --post-hook 'service nginx start'"

    - stat:
        path: "/etc/letsencrypt/live/{{ cert_domain }}/privkey.pem"
      when: cert_domain is defined
      register: __cert
    - name: Stop Nginx
      service:
        name: nginx
        state: stopped
      when: cert_domain is defined and __cert is defined and __cert.stat.exists == False
    - name: Generate letsencrypt certificate if not present
      when: cert_domain is defined
      command: "certbot certonly --standalone --email '{{ cert_email }}' -d '{{ cert_domain }}' --agree-tos --renew-by-default --keep-until-expiring"
      args:
        creates: "/etc/letsencrypt/live/{{ cert_domain }}/privkey.pem"
    - name: Start Nginx
      service:
        name: nginx
        state: started
      when: cert_domain is defined and __cert is defined and __cert.stat.exists == False

    - name: Deploys app configuration
      when: web_host is defined
      template:
        src: ../templates/virtualhost.conf.j2
        dest: /etc/nginx/sites-enabled/virtualhost.conf
        group: www-data
      notify: Restart nginx
